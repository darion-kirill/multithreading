package com.otus.study.multithreading.dto

import com.otus.study.multithreading.action.GameAction

class GameTask(private var thread: GameThread, private var commands: MutableList<GameAction>) {

    fun getGameThread() = this.thread
    fun getCommands() = this.commands

}