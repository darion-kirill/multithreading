package com.otus.study.multithreading.dto

class GameThread(private var gameId: String){

    fun getGameId() = this.gameId

}