package com.otus.study.multithreading.server

import com.otus.study.multithreading.action.GameAction
import com.otus.study.multithreading.dto.GameTask
import com.otus.study.multithreading.exception.GameActionException
import com.otus.study.multithreading.exception.HardStopException
import org.springframework.stereotype.Service
import java.util.*
import java.util.concurrent.ConcurrentLinkedQueue

@Service
class GameService {

    private val commonQueue = ConcurrentLinkedQueue<GameTask>()

    fun createNewGame(): String{
        val threadName = UUID.randomUUID().toString()

        val runnable = Runnable{ threadRun(threadName) }
        val newGame = Thread(runnable, threadName)
        newGame.start()
        println("New game created: $threadName")

        return threadName
    }

    fun registerTask(gameTask: GameTask){
        commonQueue.offer(gameTask)
        println("register task $gameTask")
    }

    fun threadRun(threadName: String){
        var threadStop = false
        while(!threadStop){
            try {
                val currentActions: MutableList<GameAction> = getNewTask(threadName)
                currentActions.forEach {
                    val actionResult = it.execute()
                    if(actionResult.first == "SoftStop"){
                        threadStop = true
                    }
                }
            }catch (e: HardStopException){
                println(e.message)
                threadStop = true
            }catch (e: GameActionException){
                println("Thread $threadName catch GameActionException ${e.message}")
            }catch (e: Exception){
                println("Thread $threadName catch unknown exception ${e.message}")
            }
        }
    }


    @Synchronized
    private fun getNewTask(currentThreadName: String): MutableList<GameAction>{
        if(commonQueue.isNotEmpty()){
            val lastTask = commonQueue.element()
            if(lastTask.getGameThread().getGameId() == currentThreadName){
                val newTask = commonQueue.poll()
                return newTask.getCommands()
            }
        }

        return mutableListOf()
    }
}