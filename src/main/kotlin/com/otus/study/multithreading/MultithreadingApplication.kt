package com.otus.study.multithreading

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class MultithreadingApplication

fun main(args: Array<String>) {
	runApplication<MultithreadingApplication>(*args)
}
