package com.otus.study.multithreading.action.impl

import com.otus.study.multithreading.action.GameActionAbstract
import org.springframework.stereotype.Component

@Component
class SoftStop: GameActionAbstract() {
    override fun execute(): Pair<String, Any?> {
        println("There was a command to soft stop the flow")
        return Pair("SoftStop", null)
    }
}