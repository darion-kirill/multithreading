package com.otus.study.multithreading.action.impl

import com.otus.study.multithreading.action.GameActionAbstract
import org.springframework.stereotype.Component

@Component
class CreateGameObject(): GameActionAbstract() {
    override fun execute(): Pair<String, Any?> {
        val message = "CreateGameObject execute with action data: ${getActionData()}"
        println(message)
        return Pair("RUN", message)
    }
}