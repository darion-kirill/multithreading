package com.otus.study.multithreading.action.impl

import com.otus.study.multithreading.action.GameActionAbstract
import com.otus.study.multithreading.exception.HardStopException
import org.springframework.stereotype.Component

@Component
class HardStop: GameActionAbstract() {
    override fun execute(): Pair<String, Any?> {
        throw HardStopException("There was a command to hard stop the flow")
    }
}