package com.otus.study.multithreading.action

import com.otus.study.multithreading.exception.NoSuchGameActionException
import org.springframework.stereotype.Service
import java.util.*

@Service
class GameActionFactory(private val availableActions: List<GameAction>) {

    fun getActionByName(actionName: String): GameAction{
        val currentBeanList = availableActions.filter{
            getShortClassName(it.javaClass.name).equals(
                actionName.lowercase(Locale.getDefault()),
                ignoreCase = true
            )
        }
        if(currentBeanList.isEmpty()){
            throw NoSuchGameActionException("Action $actionName were not found among the possible game actions for the tank-game")
        }

        return currentBeanList[0]
    }

    fun getActionList(actionNames: List<String>): List<GameAction>{
        return actionNames.map { actionName -> getActionByName(actionName) }
    }

    fun getShortClassName(className: String): String{
        return className.substring(className.lastIndexOf('.') + 1)
    }

}