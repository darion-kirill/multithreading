package com.otus.study.multithreading.action

import com.otus.study.multithreading.exception.GameActionException
import kotlin.jvm.Throws

interface GameAction {
    @Throws(GameActionException::class)
    fun execute(): Pair<String, Any?>

    fun setActionData(actionData: Map<String, String>)
}