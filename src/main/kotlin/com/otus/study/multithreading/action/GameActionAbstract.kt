package com.otus.study.multithreading.action

abstract class GameActionAbstract: GameAction {
    private var actionData = mapOf<String, String>()
    override fun setActionData(actionData: Map<String, String>) {
        this.actionData = actionData
    }
    fun getActionData() = actionData
}