package com.otus.study.multithreading.action.impl

import com.otus.study.multithreading.action.GameActionAbstract

class RotateObject: GameActionAbstract() {
    override fun execute(): Pair<String, Any?> {
        val message = "RotateObject execute with action data: ${getActionData()}"
        println(message)
        return Pair("RUN", message)
    }
}