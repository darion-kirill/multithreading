package com.otus.study.multithreading

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import com.otus.study.multithreading.action.GameAction
import com.otus.study.multithreading.action.GameActionFactory
import com.otus.study.multithreading.dto.GameTask
import com.otus.study.multithreading.dto.GameThread
import com.otus.study.multithreading.server.GameService
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping(
    path = ["/tank-game"]
)
class Endpoint(private val gameService: GameService,
               private val mapper: ObjectMapper,
               private val gameActionFactory: GameActionFactory
) {

    @RequestMapping(method = [RequestMethod.POST], path = ["/register"])
    fun registerCommand(
        @RequestHeader headers: Map<String, String>,
        @RequestBody body: String
    ){
        val gameId = headers.getOrDefault("game_id", "")
        val newTask = GameTask(GameThread(gameId), parseBody(mapper.readTree(body)))
        gameService.registerTask(newTask)
    }

    @GetMapping(path = ["/new-game"])
    fun startNewGame(): ResponseEntity<GameThread>{
        val newGame = GameThread(gameService.createNewGame())
        return ResponseEntity(newGame, HttpStatus.OK)
    }

    private fun parseBody(node: JsonNode): MutableList<GameAction>{

        val gameActions = mutableListOf<GameAction>()

        val methodElements = node.fields()
        for(element in methodElements){
            val gameAction = gameActionFactory.getActionByName(element.key)
            val actionParams = element.value.fields()
            val actionData = mutableMapOf<String, String>()
            for (actionParam in actionParams){
                actionData[actionParam.key] = actionParam.value.toString()
            }
            gameAction.setActionData(actionData)
            gameActions.add(gameAction)
        }

        return gameActions
    }

}