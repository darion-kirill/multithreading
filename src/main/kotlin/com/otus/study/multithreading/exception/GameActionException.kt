package com.otus.study.multithreading.exception

import java.io.IOException

open class GameActionException(override val message: String): IOException(message)