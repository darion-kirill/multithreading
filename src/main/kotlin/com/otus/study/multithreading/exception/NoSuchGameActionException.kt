package com.otus.study.multithreading.exception

import java.io.IOException

class NoSuchGameActionException(override val message: String): IOException(message)