package com.otus.study.multithreading.exception

class HardStopException(override val message: String): GameActionException(message)