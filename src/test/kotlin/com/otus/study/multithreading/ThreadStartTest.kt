package com.otus.study.multithreading

import com.otus.study.multithreading.server.GameService
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
class ThreadStartTest(@Autowired private val gameService: GameService) {

    @Test
    fun startTest(){

        val currentThreadName = gameService.createNewGame()
        val currentThreads = Thread.getAllStackTraces().keys
        assertTrue(currentThreads.stream().anyMatch{it.name == currentThreadName})

    }


}