package com.otus.study.multithreading

import com.otus.study.multithreading.action.impl.CreateGameObject
import com.otus.study.multithreading.action.impl.HardStop
import com.otus.study.multithreading.action.impl.MoveObject
import com.otus.study.multithreading.action.impl.SoftStop
import com.otus.study.multithreading.dto.GameTask
import com.otus.study.multithreading.dto.GameThread
import com.otus.study.multithreading.server.GameService
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import java.util.*

@SpringBootTest
class ThreadStopTest(@Autowired private val gameService: GameService) {

    @Test
    fun softStopTest(){
        val currentThreadName = UUID.randomUUID().toString()

        val createGameObject = Mockito.spy(CreateGameObject::class.java)
        val moveObject = Mockito.spy(MoveObject::class.java)
        val softStop = Mockito.spy(SoftStop::class.java)

        val task = GameTask(
            GameThread(currentThreadName),
            mutableListOf(createGameObject, softStop, moveObject)
        )
        gameService.registerTask(task)
        gameService.threadRun(currentThreadName)

        Mockito.verify(createGameObject, Mockito.times(1)).execute()
        Mockito.verify(moveObject, Mockito.times(1)).execute()
    }

    @Test
    fun hardStopTest(){
        val currentThreadName = UUID.randomUUID().toString()

        val createGameObject = Mockito.spy(CreateGameObject::class.java)
        val moveObject = Mockito.spy(MoveObject::class.java)
        val hardStop = Mockito.spy(HardStop::class.java)

        val task = GameTask(
            GameThread(currentThreadName),
            mutableListOf(createGameObject, hardStop, moveObject)
        )
        gameService.registerTask(task)
        gameService.threadRun(currentThreadName)

        Mockito.verify(createGameObject, Mockito.times(1)).execute()
        Mockito.verifyNoInteractions(moveObject)
    }

}